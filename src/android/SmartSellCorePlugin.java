package com.enparadigm.smartsellcoreplugin;

import android.util.Log;

import com.smartsell.SmartSell;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class SmartSellCorePlugin extends CordovaPlugin {

    static String TAG = "SmartSellCorePlugin";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initialize")) {
            SmartSell.INSTANCE.createSmartSellEngine(this.cordova.getActivity());
            callbackContext.success("SDK initialized successfully");
            return true;
        } else if (action.equals("setData")) {
            Log.d(TAG , "setData args.getString(0) : " + args.getString(0));
            String json = args.getString(0);
            Log.d(TAG , "setData json : " + json);
            SmartSell.INSTANCE.initialize(this.cordova.getActivity(), json, () -> callbackContext.success("SDK dat set successfully"), s -> callbackContext.error("Error while setting SDK Data: " + s));
            return true;
        } else if (action.equals("open")) {
            SmartSell.INSTANCE.open(this.cordova.getActivity());
            callbackContext.success("SDK Opened successfully");
            return true;
        } else if (action.equals("show_notification")) {
            String json = args.getString(0);
            SmartSell.INSTANCE.showNotification(this.cordova.getActivity(),json);
            callbackContext.success("Notification shown successfully");
            return true;
        } else if (action.equals("clear_data")) {
            SmartSell.INSTANCE.clearData(this.cordova.getActivity());
            callbackContext.success("SDK data cleared successfully");
            return true;
        }
        return false;
    }
}
