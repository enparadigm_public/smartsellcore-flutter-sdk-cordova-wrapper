
# SmartsellCore Flutter SDK Cordova Wrapper

## Pre-requirement
Our SDK uses AndroidX libraries in its core. So you need to migrate your app to AndroidX by installing `cordova-plugin-androidx` plugin or set `<preference name="AndroidXEnabled" value="true" />` in your project `config.xml` file. Also make sure to add androidVersionCode by adding **`android-versionCode="1"`** in your config.xml files `widget` tag. Example shown below.

```
<?xml version='1.0' encoding='utf-8'?>
<widget id="com.enparadigm.smartsellCore" android-versionCode="1" version="1.0.0" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">
```

## Remove old plugin if already install and add the new plugin
```sh
$ cd CORDOVA_PROJECT
$ cordova plugin remove com.enparadigm.smartsellcoreplugin
$ cordova plugin add https://gitlab.com/enparadigm-mobile/smartsellcore-flutter-sdk-cordova-wrapper#main
```
**Note:** The plugin uses the SDK which we are present in private repository. So you need to add following lines in your gradle.properties file.
```
artifactory_repo_smart_sell_core_release_url=ARTIFACTORY_URL_GIVEN_BY_US
artifactory_username=USERNAME_GIVEN_BY_US
artifactory_password=PASSWORD_GIVEN_BY_US
```

## Plugin functions
There are 5 functions in the plugin,
 1. **initialize** used to initialise the SDK - So we its better to call this function on app start.
 2. **setData** used to set the data required by the SDK eg, username, companyId, etc.,
 3. **open** will open the SmartSell homepage.
 6. **showNotification** will display the appropriate notifications.
 7. **clearData** will clear existing data from SDK. Should be called during logout.
 
**Note:** Make sure to call `initialize` and then `setData` before calling any other SDK functions. 


## Sample code to call plugin function
#### 1. `initialize` Function
```javascript
// used to initialize the object required by the SDK. So we its better to call this function on app start
var  plugin = new  SmartSellCorePlugin();
plugin.initialize(function (msg) {
    console.log("initialize success");
},
function (err) {
    console.log("initialize fail " + err);
})
```

#### 2. `setData` Function
```javascript
// making the json required by `setData` function
var obj = new Object();
obj.base_url = "API_URL_GIVEN_BY_US"; // String - Mandatory
obj.company_unique_id = "COMPANY_UNIQUE_ID_GIVEN_BY_US"; // String - Mandatory
obj.user_unique_id = userId; // String - Mandatory
obj.user_group_id = parseInt(userGroupId); // Int - Mandatory
obj.country_code = null; // String - Optional
obj.user_meta = null; // Json String - Optional
obj.name = name; // String - Optional
obj.mobile_number = mobileNumber; // String - Optional
obj.email = email; // String - Optional
let jsonString = String(JSON.stringify(obj));

// Calling `setData` function
var plugin = new SmartSellCorePlugin();
plugin.setData(jsonString,function (msg) {
    console.log("setData success");
},
function (err) {
    console.log("setData fail " + err);
})

// Calling `open` function
var plugin = new SmartSellCorePlugin();
plugin.open(function (msg) {
    console.log("setData success");
},
function (err) {
    console.log("setData fail " + err);
})
```
#### 3. `open` Function
```javascript
// used to open smartsell home page
var  plugin = new  SmartSellCorePlugin();
plugin.open(function (msg) {
    console.log("initialize success");
},
function (err) {
    console.log("initialize fail " + err);
})
```
#### 4. `showNotification` Function
```javascript
// used to show appropritate notification. 
cordova.plugins.firebase.messaging.onMessage(function(payload) {
    let json = JSON.stringify(payload);
    console.log("New foreground FCM message: ", json);
    var plugin = new SmartSellCorePlugin();
    plugin.showNotification(json,function (msg) {
        console.log("showNotification success");
    },
    function (err) {
        console.log("showNotification fail " + err);
    })
});
```
#### 5. `clearData` Function
```javascript
// used to clear SDK data. We need to call this function when parent app user logs out.
var  plugin = new  SmartSellCorePlugin();
plugin.clearData(function (msg) {
    console.log("initialize success");
},
function (err) {
    console.log("initialize fail " + err);
})
```