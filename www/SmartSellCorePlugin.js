var exec = require('cordova/exec');

var PLUGIN_NAME = "SmartSellCorePlugin";
var SmartSellCorePlugin = function () {
    console.log('SmartSellCorePlugin instanced');
};

SmartSellCorePlugin.prototype.initialize = function (onSuccess, onError) {
    var errorCallback = function (obj) {
        onError(obj);
    };

    var successCallback = function (obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'initialize', []);
};

SmartSellCorePlugin.prototype.setData = function (json, onSuccess, onError) {
    var errorCallback = function (obj) {
        onError(obj);
    };

    var successCallback = function (obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'setData', [json]);
};

SmartSellCorePlugin.prototype.open = function (onSuccess, onError) {
    var errorCallback = function (obj) {
        onError(obj);
    };

    var successCallback = function (obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'open', []);
};

SmartSellCorePlugin.prototype.showNotification = function (notificationJson, onSuccess, onError) {
    var errorCallback = function (obj) {
        onError(obj);
    };

    var successCallback = function (obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'show_notification', [notificationJson]);
};

SmartSellCorePlugin.prototype.clearData = function (onSuccess, onError) {
    var errorCallback = function (obj) {
        onError(obj);
    };

    var successCallback = function (obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'clear_data', []);
};

if (typeof module != 'undefined' && module.exports) {
    module.exports = SmartSellCorePlugin;
}